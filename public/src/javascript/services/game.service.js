import callWebApi from '../helpers/api.helper';

export const getText = async () => {
    const response = await callWebApi({
        endpoint: '/api/game/text',
        type: 'GET',
    });
    return response.json();
};