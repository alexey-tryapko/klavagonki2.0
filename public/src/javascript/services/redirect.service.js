import AuthView from '../views/auth.view';
import GameView from '../views/game.view';

const rootElement = document.getElementById('root');

const clearRoot = () => {
    while (rootElement.firstChild) {
        rootElement.removeChild(rootElement.firstChild);
    };
}

export const auth = () => {
    clearRoot();
    const authView = new AuthView();
    const authElement = authView.element;
    rootElement.appendChild(authElement);
};

export const game = () => {
    clearRoot();
    const gameView = new GameView();
    const gameElement = gameView.element;
    rootElement.appendChild(gameElement);
}