import * as redirect from './services/redirect.service';

class App {
    constructor() {
        this.startApp();
    }

    static rootElement = document.getElementById('root');

    async startApp() {
        try {
            const token = localStorage.getItem('token');
            if (token) {
                redirect.game();
            } else {
                redirect.auth();
            }

        } catch (error) {
            console.warn(error);
            App.rootElement.innerText = 'Failed to load data';
        } finally {
            // App.loadingElement.style.visibility = 'hidden';
        }
    }
}

export default App;