import View from './view';
import io from 'socket.io-client';
import * as redirect from '../services/redirect.service';
import * as gameService from '../services/game.service';

class GameView extends View {
    constructor() {
        super();
        this.token = localStorage.getItem('token');
        this.createLobbyWindow = this.createLobbyWindow.bind(this);
        this.createGameWindow = this.createGameWindow.bind(this);
        this.clearElement = this.clearElement.bind(this);
        this.updateLeftToGameEndTime = this.updateLeftToGameEndTime.bind(this);
        this.removeGameField = this.removeGameField.bind(this);
        this.updatePlayers = this.updatePlayers.bind(this);
        this.createText = this.createText.bind(this);
        this.createProgressBar = this.createProgressBar.bind(this);
        this.handlePressKey = this.handlePressKey.bind(this);
        this.updateText = this.updateText.bind(this);
        this.updateComment = this.updateComment.bind(this);
        this.socket = null;
        this.currIndex = 0;
        this.stat = null;
        this.text = '';
        this.createGame();
    }
    static rootElement = document.getElementById('root');

    async createGame() {
        if (!this.token) {
            redirect.auth();
        } else {
            this.element = this.createElement({ tagName: 'div', classNames: ['game-wrapper'] });

            const socket = io.connect('http://localhost:3007', { query: { token: this.token } });
            this.socket = socket;
            this.updateText();
            socket.on('timeLeftToGameStart', this.createLobbyWindow);
            socket.on('timeLeftTOGameEnd', this.updateLeftToGameEndTime);
            socket.on('startGame', this.createGameWindow.bind(null, this.text));
            socket.on('endGame', this.removeGameField);
            socket.on('players', this.updatePlayers);
            socket.on('comment', this.updateComment);
        }
    }
    updateComment({ comment }) {
        const list = this.element.querySelector('.comments-list');
        const li = this.createElement({tagName: 'li', classNames: ['comments-item']});
        li.innerHTML = comment.text;
        list.prepend(li);
    }

    handlePressKey({ keyCode }) {
        if (String.fromCharCode(keyCode) === this.text[this.currIndex]) {
            this.currIndex++;
            this.higlightText(this.currIndex);
            this.socket.emit('enterChar', { token: this.token });
            if (this.text.length === this.currIndex) {
                this.socket.emit('finished', { token: this.token });
            }
        }
  
    };

    createPlayersList(players) {
        const list = this.createElement({ tagName: 'ul', classNames: ['list-group', 'list', 'list-group-flush', 'players-list'] });
        const listItems = players.map(({ email, enteredSymbols, active }) => {
            const status = active ? 'online' : 'notActive';
            const item = this.createElement({ tagName: 'li', classNames: ['list-group-item', status] });
            const nameBlock = this.createElement({ tagName: 'p', classNames: ['name'] });

            nameBlock.innerText = email;
            const progressbar = this.createProgressBar(enteredSymbols);
            item.append(nameBlock, progressbar);
            return item;
        });
        list.append(...listItems);
        return list;
    };

    createProgressBar(enteredSymbols) {
        const wrapper = this.createElement({ tagName: 'div', classNames: ['progress'] });
        const bar = this.createElement({
            tagName: 'div',
            classNames: ['progress-bar'],
            attributes: {
                role: 'progressbar'
            }
        });
        const textLength = this.text.length;
        bar.style.width = `${Math.floor((enteredSymbols * 100) / textLength)}%`;
        wrapper.append(bar);
        return wrapper;
    };

    createText() {
        const paragraph = this.createElement({ tagName: 'p', classNames: ['game-text'] });
        paragraph.innerText = this.text;
        return paragraph;
    }

    createLobbyWindow({ time }) {
        if (this.stat) {
            this.updateLobbyWinodw({ time });
            return;
        }
        this.clearElement();
        const lobbyCountStart = this.createElement({ tagName: 'h1', classNames: ['lobby-header'] });
        lobbyCountStart.innerText = `The next game starts in: ${time ? time : ''}s`;
        this.element.appendChild(lobbyCountStart);
    };

    updateLobbyWinodw({ time }) {
        this.removeGameField();
        let lobbyCountStart = this.element.querySelector('.lobby-header');
        if (!lobbyCountStart) {
            const lobbyCountStart = this.createElement({ tagName: 'h1', classNames: ['lobby-header'] });
            lobbyCountStart.innerText = `The next game starts in: ${time ? time : ''}s`;
            this.element.appendChild(lobbyCountStart);
        } else {
            lobbyCountStart.innerText = `The next game starts in: ${time ? time : ''}s`;
        }
    }

    createGameWindow(text, { players }) {
        this.clearElement();
        document.body.addEventListener('keypress', this.handlePressKey);
        const textBlock = this.createText(text);
        const list = this.createPlayersList(players);
        const countDown = this.createElement({ tagName: 'p', classNames: ['gameCountDown'] });
        const commentsList = this.createElement({ tagName: 'ul', classNames: ['comments-list']});
        this.element.append(list, commentsList, textBlock, countDown);
        this.higlightText(0);
    }

    updateLeftToGameEndTime({ time }) {
        const item = this.element.querySelector('.gameCountDown');
        item.innerText = `Until the end: ${time}s`;
    }
    async updateText() {
        const { text } = await gameService.getText();
        this.text = text;
    }
    removeGameField() {
        document.body.removeEventListener('keypress', this.handlePressKey);
        this.currIndex = 0;
        this.stat = this.element.querySelector('.players-list');
        this.comments = this.element.querySelector('.comments-list');
        this.clearElement();
        this.element.prepend(this.stat, this.comments);
        this.updateText();
    }

    clearElement() {
        while (this.element.firstChild) {
            this.element.removeChild(this.element.firstChild);
        };
    };

    updatePlayers({ players }) {
        const newStat = this.createPlayersList(players);
        const oldStat = this.element.querySelector('.players-list');
        if(oldStat) {
            this.element.replaceChild(newStat, oldStat);
            this.element.prepend(newStat);
        }

    };

    higlightText(currCharIndex) {
        const text = this.element.querySelector('.game-text');
        const enteredTextElement = this.createElement({ tagName: 'span', classNames: ['red-highlight'] });
        const enteredText = this.text.slice(0, currCharIndex);
        enteredTextElement.innerText = enteredText;
        const currCharElement = this.createElement({ tagName: 'span', classNames: ['green-highlight'] });
        const currChar = this.text[currCharIndex] || '';
        currCharElement.innerText = currChar;
        const restTextElement = this.createElement({ tagName: 'span' });
        const rest = this.text.slice(currCharIndex + 1);
        restTextElement.innerText = rest;

        while (text.firstChild) {
            text.removeChild(text.firstChild);
        };

        text.append(enteredTextElement, currCharElement, restTextElement);

    }
};

export default GameView;