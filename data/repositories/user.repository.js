import BaseRepository from './base.repository';

class UserRepository {
    constructor() {
        this.usersStorage = BaseRepository("users.json");
    }

    addUser(user) {
        return this.usersStorage.addData(user);
    }

    getByEmail(email) {
        return this.usersStorage.getDataByField('email', email);
    }

    getByUsername(username) {
        return this.usersStorage.getDataByField('username', username);
    }

    getUserById(id) {
        return this.usersStorage.getDataById(id);
    }
}

export default new UserRepository();
