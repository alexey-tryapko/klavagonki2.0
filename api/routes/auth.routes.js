import { Router } from 'express';
import * as authService from '../services/auth.service';
import authenticationMiddleware from '../middlewares/authentication.middleware';

const router = Router();

router
  .post('/login', authenticationMiddleware, (req, res, next) => authService.login(req.body)
    .then(data => res.send(data))
    .catch(next));

export default router;
