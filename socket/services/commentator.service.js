import { messageFactory } from './messageFactory';
//Facade
class CommentsService {
    constructor(messageFactory) {
        this.messageFactory = messageFactory;
        this.greating = this.greating.bind(this);
        this.initPlayers = this.initPlayers.bind(this);
        this.currentState = this.currentState.bind(this);
        this.preFinish = this.preFinish.bind(this);
        this.playerFinished = this.playerFinished.bind(this);
        this.gameEnded = this.gameEnded.bind(this);
        this.farewell = this.farewell.bind(this);
        this.randomStories = this.randomStories.bind(this);

    };
    greating(name) {
        const text = `Hello my name is ${name} I will be your commentator`;
        const author = name;
        return this.messageFactory.create(text, author);
    };
    initPlayers(players, name = 'Igor') {
        const text = players.reduce((acc, { email }) => (
            acc + `Player ${email} on the starting lane` + '<br/>'
        ), '');
        return this.messageFactory.create(text, name);
    };
    currentState(players, { text: gameText }, name = 'Igor') {
        const totalChars = gameText.length;
        const text = players.reduce((acc, { email, enteredSymbols }, i) => (
            acc + `${i + 1}. ${email}  ${totalChars - enteredSymbols} chars lefts<br/>`
        ), '');
        return this.messageFactory.create(text, name);
    };
    preFinish({ email }, name = 'Igor') {
        const text = `${email} at the finish line`;
        return this.messageFactory.create(text, name);
    };
    playerFinished({ email }, name = 'Igor') {
        const text = `${email} finished`;
        return this.messageFactory.create(text, name);
    };
    gameEnded(players, name = 'Igor') {
        const text = players.reduce((acc, { email, usedTime }, i) => (
            acc + `${i + 1}. ${email}  ${usedTime || ''}<br/>`
        ), '');
        return this.messageFactory.create(text, name);
    };
    farewell(name) {
        const text = `incredible game over. With you was your ${name}`;
        const author = name;
        return this.messageFactory.create(text, author);
    };
    randomStories() {

    };
};

export default new CommentsService(messageFactory);