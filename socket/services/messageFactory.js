//Factory
class Message {
  constructor(text, author) {
    this.text = text;
    this.author = author;
  }
}

class Author {
  constructor(name) {
    this.name = name;
  }
}

export const messageFactory = {
  create(text, name) {
    const author = new Author(name);
    const message = new Message(text, author);
    return message;
  }
}

