import { io } from './../server';

export default function (game, socket) {
    socket.join('lobby');
    if (io.engine.clientsCount === 1) {
        game.init();
    }

    socket.on('disconnect', () => {
        if (!io.engine.clientsCount) {
            game.reset();
        }
    });
};
