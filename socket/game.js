export default class Game {
    constructor() {
        this.breakDuration = 15;
        this.duration = 60;
        this.players = [];
        this.isPlaying = false;
    };

    setPlayers(newPlayers) {
        const finished = newPlayers
            .filter(player => player.usedTime !== 0)
            .sort((first, second) => first.usedTime - second.usedTime);

        const playing = newPlayers
            .filter(player => player.usedTime === 0)
            .sort((first, second) => {
                return second.enteredSymbols - first.enteredSymbols;
            });
        this.players = [...finished, ...playing];
        return this.players;
    }

    setfinishedUsers(value) {
        this.finishedUsers = value;
        if(this.finishedUsers === this.players.length) this.stop();
    };

    finishedUsersCount() {
        const res = this.players.reduce((acc, curr) => {
            const { usedTime, active } = curr;
            acc += (usedTime ? 1 : 0) || (active ? 0 : 1);
            return acc;
        }, 0);
        this.setfinishedUsers(res);
    }

    getPlayerIndex(email) {
        return this.players.findIndex(user => user.email === email);
    }

    setPlayerNotActive(email) {
        const index = this.getPlayerIndex(email);
        this.players[index].active = false;
        this.finishedUsersCount();
        this.setPlayers(this.players);
    }

    increasePlayerEnteredSymbols(email) {
        const index = this.getPlayerIndex(email);
        this.players[index].enteredSymbols++;
        this.setPlayers(this.players);
        return this.players[index].enteredSymbols;
    }

    setPlayerFinished(email) {
        const index = this.getPlayerIndex(email);
        this.players[index].usedTime = this.duration - this.timeLeftToEnd;
        this.finishedUsersCount();
        this.setPlayers(this.players);
    }

    decreaseTimeLeftToStart() {
        --this.timeLeftToStart;
        if (!this.timeLeftToStart) this.start();
    }

    decreaseTimeLeftToEnd() {
        if (!this.isPlaying) return;
        --this.timeLeftToEnd;
        if (!this.timeLeftToEnd) this.stop();
    }

    generateTextID() {
        return Math.floor(Math.random() * Math.floor(4));
    }

    setTime() {
        this.timeLeftToStart = this.isPlaying ?
            (this.breakDuration + this.duration) :
            this.breakDuration;
        this.timeLeftToEnd = this.duration;
    }

    startTimer() {
        this.timer = setInterval(() => {
            this.decreaseTimeLeftToStart();
            this.decreaseTimeLeftToEnd();
        }, 1000);
    }

    reset() {
        clearInterval(this.timer);
        this.isPlaying = false;
        this.setTime();
    }

    init() {
        this.setTime();
        this.texID = this.generateTextID();
        this.startTimer();
    }

    start() {
        this.texID = this.generateTextID();
        this.isPlaying = true;
        this.setTime();
    }

    stop() {
        this.isPlaying = false;
        this.setTime();
    }
};